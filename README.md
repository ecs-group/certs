# README #

### What is this repository for?
This is a simple python program that scrapes the following URL.  
https://foresttechnologies.atlassian.net/wiki/spaces/DP/pages/2795831378/Certs

It then creates an Excel spreadsheet that contains a count of the current and planned certifications as below. 

### Pre-requisites
You need to create your own token in confluence to pass as a parameter.
https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html 

### Running The Program. 
The program takes 3 arguments as below.  You will need to use your ecs email and confluence token.

```
cert_rest_api_counter.py
--url=http://foresttechnologies.atlassian.net/wiki/rest/api/content/2795831378?expand=body.storage  
--user=john.dobie@ecs.co.uk 
--token=cM6uUiwlrJcy.....
```

### Improvements
- refactor the code to remove globals
- add additional reports - ask JB.

### Output
output_20210224-102119.xlsx
```
['AWS Business Professional', 6, 0, 6]
['AWS Certified Architect Associate', 4, 8, 12]
['AWS Certified Architect Professional ', 2, 1, 3]
['AWS Certified Big Data Speciality', 1, 1, 2]
['AWS Certified Cloud Practitioner', 15, 3, 18]
['AWS Certified Data Analytics Speciality', 0, 2, 2]
['AWS Certified Database Speciality', 0, 1, 1]
['AWS Certified Developer Associate', 1, 3, 4]
['AWS Certified Machine Learning Speciality', 0, 3, 3]
['AWS MLOps bootcamp checkride', 0, 5, 5]
['AWS Managed Services Foundations', 1, 0, 1]
['AWS Quicksight', 2, 1, 3]
['AWS Technical professional', 6, 0, 6]
['AWS Well Architected Lead', 2, 1, 3]
['AWS-SCS', 0, 1, 1]
['AWS-SOA', 1, 0, 1]
['Agile Methodology', 1, 1, 2]
['Amazon Connect - Business', 1, 0, 1]
['BCS Certificate in Business Analysis\xa0', 1, 1, 2]
['BCS Certificate in Requirements Engineering\xa0', 1, 0, 1]
['BCS Certificate in\xa0Modelling Business\xa0Processes', 0, 1, 1]
['BCS SIAM Foundation', 2, 0, 2]
['Certified Agile Service Manager', 4, 0, 4]
['Certified ServiceNow System Administrator', 1, 0, 1]
['DataRobot', 0, 4, 4]
['Elasticsearch', 0, 2, 2]
['FAIS Supervision & Guidelines', 1, 0, 1]
['Functional Programming Principles in Scala', 1, 0, 1]
['GCP Associate Cloud Engineer ', 1, 0, 1]
['GCP Professional Cloud Architect', 3, 0, 3]
['HashiCorp Certified Terraform Associate', 1, 0, 1]
['ITIL 3 - Expert, Managing Across The Lifecycle', 2, 0, 2]
['ITIL 4 - Certified Digital Leader and IT Strategy', 0, 1, 1]
['ITIL 4 - Certified Managing Professional', 3, 0, 3]
['ITIL V3 - Foundation Certificate', 2, 0, 2]
['Leading Safe', 1, 0, 1]
['MLOPS-TRAINING', 0, 1, 1]
['Mentoring in the Workplace', 1, 0, 1]
['Microsoft Certified Azure Fundamentals', 1, 0, 1]
['PCAP - Certified Associate in Python Programming', 0, 1, 1]
['PCEP -  Certified Entry-Level Python Programmer', 0, 1, 1]
['PCPP - Certified Professional in Python Programming', 0, 1, 1]
['Powerbi ', 0, 1, 1]
['Prince2 Practictioner', 1, 0, 1]
['SAFe - Scrum Master', 0, 2, 2]
['SIAM', 1, 0, 1]
['SPLUNK-SENG', 2, 2, 4]
['SPLUNK-SENG2,SPLUNK-SENG3', 1, 0, 1]
['SQL Server Reporting Services (SSRS)\xa0', 1, 0, 1]
['Scrum Alliance - Certified ScrumMaster', 2, 1, 3]
['Snowflake WEBUI Essentials', 1, 0, 1]
['Splunk Advanced Dashboards and Visualizations\xa0', 4, 1, 5]
['Splunk Advanced Searching and Reporting\xa0', 4, 1, 5]
['Splunk Certified Admin', 4, 2, 6]
['Splunk Certified Architect', 2, 1, 3]
['Splunk Certified Core Consultant', 2, 1, 3]
['Splunk Certified ITSI administrator', 1, 2, 3]
['Splunk Certified Power User', 6, 2, 8]
['Splunk Certified User', 6, 1, 7]
['Splunk Enterprise Data Administration\xa0', 5, 1, 6]
['Splunk Enterprise System Administration\xa0', 5, 1, 6]
['Splunk For Analytics and Data Science', 1, 0, 1]
['Splunk ITSI SME Level 1', 3, 1, 4]
['Splunk Implementing Splunk IT Service Intelligence\xa0', 4, 0, 4]
['Splunk Sales Development', 1, 0, 1]
['Splunk Sales Engineer I', 2, 0, 2]
['Splunk Sales Engineer II', 1, 0, 1]
['Splunk Service User', 2, 1, 3]
['Splunk certified cloud administrator', 0, 3, 3]
['WANdisco', 0, 1, 1]
```