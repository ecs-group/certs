#  Reads the data from https://foresttechnologies.atlassian.net/wiki/spaces/DP/pages/2795831378/Certs
#  Then sums the number of certs currently held and planned.
#  Outputs the results as an Excel Spreadsheet.
#
import argparse
import operator
import time
from collections import Counter

import requests
import xlsxwriter
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth

CERTS_FILENAME = 'certs.xls'

def get_cert_count(user_certs):
    c = Counter();

    for cert_list in user_certs:
        for certs in cert_list.replace(', ', ' ').split():
            c[lookup_course_name_by_key(certs)] += 1;

    return c


def lookup_course_name_by_key(key):
    if key in course_name_lookups.keys():
        return course_name_lookups[key]

    return key


def sort_dict(cert_counter):
    sorted_certs = dict(sorted(cert_counter.items(), key=operator.itemgetter(0), reverse=False))
    return sorted_certs


def print_certs(cert_counter):
    sorted_certs = sort_dict(cert_counter)

    for key, value in sorted_certs.items():
        print(key, ",", value)


def get_total(key, counter):
    if key in counter:
        return counter.get(key)

    return 0;


def get_html(url, user, token):
    r = requests.get(url, auth=HTTPBasicAuth(user, token))

    json_string = r.json()
    html_code = json_string['body']['storage']['value']

    return html_code


def get_user_certs():
    html_code = get_html(url, user, token)

    results = BeautifulSoup(html_code, 'html.parser')

    tables = results.find_all('table')
    rows = tables[0].findChildren(['tr'])

    codes = dict()
    categories = dict()

    for row in rows:
        cells = row.findChildren('p')
        value = cells[0].string
        code = cells[1].string
        category = cells[2].string

        codes[code] = value
        categories[code] = category

    return (codes, categories)


def get_lookups():
    html_code = get_html(url, user, token)

    results = BeautifulSoup(html_code, 'html.parser')

    tables = results.find_all('table')
    rows = tables[1].findChildren(['tr'])

    codes = dict()
    categories = dict()

    for row in rows[1:]:
        cells = row.findChildren('p')
        value = cells[0].string
        code = cells[1].string
        category = cells[2].string

        codes[code] = value
        categories[code] = category

    return (codes, categories)


def get_user_certs_list(column):
    html_code = get_html(url, user, token)
    results = BeautifulSoup(html_code, 'html.parser')

    tables = results.find_all('table')
    rows = tables[0].findChildren(['tr'])

    certs = list()

    for row in rows[1:]:
        cells = row.findChildren('p')
        value = cells[column].string
        if value:
            certs.append(value)

    return certs


def write_excel(current_cert_counter, planned_cert_counter, total_cert_counter):
    filename = "output_" + time.strftime("%Y%m%d-%H%M%S") + ".xlsx"

    workbook = xlsxwriter.Workbook(filename)
    worksheet = workbook.add_worksheet()
    columns = ['Name', 'Current', 'Planned', 'Total']
    worksheet.write_row(0, 0, columns)
    sorted_total_certs = sort_dict(total_cert_counter)
    row = 1
    for key in sorted_total_certs.keys():
        current = get_total(key, current_cert_counter)
        planned = get_total(key, planned_cert_counter)
        total = get_total(key, total_cert_counter)
        values = [key, current, planned, total]
        print(values)
        worksheet.write_row(row, 0, values)
        row = row + 1
    workbook.close()


def main():
    parser = argparse.ArgumentParser(description='Create a report of certs in the practice.  ')

    parser.add_argument('--token', required=True, type=str,
                        help="the confluence api token. See 'https://confluence.atlassian.com/enterprise/using-personal-access-tokens-1026032365.html'")
    parser.add_argument('--url', required=True, type=str,
                        help='The confluence certs page - for example : https://foresttechnologies.atlassian.net/wiki/spaces/DP/pages/2795831378/Certs')
    parser.add_argument('--user', required=True, type=str, help='your username - for example john.dobie@ecs.co.uk')

    args = parser.parse_args()

    global token
    global user
    global url

    token = args.token
    user = args.user
    url = args.url

    global course_name_lookups
    global course_category_lookups

    course_name_lookups, course_category_lookups = get_lookups()

    print(token, user, url)

    current_certs = get_user_certs_list(2)
    planned_certs = get_user_certs_list(3)

    current_cert_counter = get_cert_count(current_certs)
    planned_cert_counter = get_cert_count(planned_certs)
    total_cert_counter = current_cert_counter + planned_cert_counter

    write_excel(current_cert_counter, planned_cert_counter, total_cert_counter)


if __name__ == '__main__':
    main()
